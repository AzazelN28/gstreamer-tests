# DASH Streaming Server test

This is a _proof of concept_ project capable of serve streamable `mp4` DASH files using GStreamer,
Node.js and other tools like Bento4 or GPAC.

## Dependencies

- [Bento4](https://www.bento4.com/)
- [GPAC](https://gpac.wp.imt.fr/)
- [Node.js](https://nodejs.org/)
- [GStreamer](https://gstreamer.freedesktop.org/)

## Installation

### GStreamer

on Ubuntu:

```sh
apt install gstreamer1.0-0 gstreamer1.0-libav gstreamer1.0-plugins-base gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-plugins-good
```

on Mac OS X:

```sh
brew install gstreamer gst-libav gst-plugins-base gst-plugins-bad gst-plugins-ugly gst-plugins-good
```

### Bento4

You can grab a precompiled version from [Bento4](https://www.bento4.com/downloads/)
or download its source code and compile it.

### GPAC

You can grab a precompiled version from [GPAC](https://gpac.wp.imt.fr/downloads/gpac-nightly-builds/)
or download its source code and compile it.

### Node.js

#### Install script

To install or update nvm, you can use the install script using cURL:

```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```

or Wget:

```sh
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```

The script clones the nvm repository to `~/.nvm` and adds the source line to
your profile (`~/.bash_profile`, `~/.zshrc`, `~/.profile`, or `~/.bashrc`).

```sh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
```

## How to run it

Execute this to start the development server:

```sh
npm start
```

This will start a local server on [](http://localhost:4000/) exposing the
following urls: [](http://localhost:4000/stream.mpd).

## How it works

It runs a local server on port `4000` using [express](http://expressjs.com),
executes `gst-launch-1.0` defining a pipeline that creates streamable `mp4`
files in the `out` folder. It also uses the first generated video (video_0.mp4)
to obtain certain information about how it is encoded using `mp4info` (a `bento4`
tool) and then generates the MPD (Media Presentation Description) available
in `http://localhost:4000/stream.mpd`.

## About MPD

MPD is a very complex specification that allows tons of different configurations,
from simple streaming solutions to complex VOD + Ads + Live streaming solutions.

TODO: Add more info to this part.
