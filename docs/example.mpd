<?xml version="1.0" encoding="utf-8"?>
<!--

- availabilityStartTime: For @type='dynamic' this attribute shall be present. In this case it specifies the anchor for the computation of the earliest availability time (in UTC) for any Segment in the Media Presentation.
- availabilityEndTime:
- maxSegmentDuration: OPTIONAL. Specifies the maximum duration of any Segment in any Representation in the Media Presentation, i.e. documented in this MPD and any future update of the MPD. If not present, then the maximum Segment duration shall be the maximum duration of any Segment documented in this MPD.
- minBufferTime: specifies the maximum duration of any Segment in any Representation in the Media Presentation, i.e. documented in this MPD and any future update of the MPD. If not present, then the maximum Segment duration shall be the maximum duration of any Segment documented in this MPD.
- minimumUpdatePeriod: OPTIONAL. If this attribute is present, it specifies the smallest period between potential changes to the MPD. This can be useful to control the frequency at which a client checks for updates.
If this attribute is not present it indicates that the MPD does not change.
If MPD@type is ‘static’, @minimumUpdatePeriod shall not be present.
Details on the use of the value of this attribute are specified in 5.4.
- publishTime: specifies the wall-clock time when the MPD was generated and published at the origin server. MPDs with a later value of @publishTime shall be an update as defined in 5.4 to MPDs with earlier @publishTime.
- timeShiftBufferDepth: OPTIONAL. specifies the duration of the smallest time shifting buffer for any Representation in the MPD that is guaranteed to be available for a Media Presentation with type 'dynamic'. When not present, the value is infinite. This value of the attribute is undefined if the type attribute is equal to ‘static’.
- type: specifies the type of the Media Presentation. For static Media Presentations (@type="static") all Segments are available between the @availabilityStartTime and the @availabilityEndTime. For dynamic Media Presentations (@type="dynamic") Segments typically have different availability times. For details refer to 5.3.9.5.3.

-->
<MPD
  availabilityStartTime="1970-01-01T00:00:00Z"
  id="Config part of url maybe?"
  maxSegmentDuration="PT6S"
  minBufferTime="PT2S"
  minimumUpdatePeriod="P100Y"
  profiles="urn:mpeg:dash:profile:isoff-live:2011,urn:com:dashif:dash264"
  publishTime="2017-08-22T11:24:11Z"
  timeShiftBufferDepth="PT5M"
  type="dynamic"
  ns1:schemaLocation="urn:mpeg:dash:schema:mpd:2011 DASH-MPD.xsd"
  xmlns="urn:mpeg:dash:schema:mpd:2011"
  xmlns:ns1="http://www.w3.org/2001/XMLSchema-instance">
  <!-- Metadata -->
  <ProgramInformation>
    <!-- Title -->
    <Title>Media Presentation Description by MobiTV. Powered by MDL Team@Sweden.</Title>
  </ProgramInformation>

  <!-- Base URL -->
  <BaseURL>https://vm2.dashif.org/livesim/utc_head/testpic_6s/</BaseURL>
  <!-- Period p0 -->
  <Period start="PT0S" ns2:publishTime="1970-01-01T00:00:00Z" xmlns:ns2="urn:mobitv">
    <!-- This is for audio (right now we don't need this) -->
    <!--
      <AdaptationSet contentType="audio" lang="eng" mimeType="audio/mp4" segmentAlignment="true" startWithSAP="1" ns2:qualityLevel="SD">
        <Role schemeIdUri="urn:mpeg:dash:role:2011" value="main"/>
        <SegmentTemplate duration="6" initialization="$RepresentationID$/init.mp4" media="$RepresentationID$/$Number$.m4s" startNumber="0"/>
        <Representation audioSamplingRate="48000" bandwidth="48000" codecs="mp4a.40.2" id="A48">
          <AudioChannelConfiguration schemeIdUri="urn:mpeg:dash:23003:3:audio_channel_configuration:2011" value="2"/>
        </Representation>
      </AdaptationSet>
    -->

    <!-- Video content -->
    <AdaptationSet contentType="video" maxFrameRate="60/2" maxHeight="360" maxWidth="640" mimeType="video/mp4" minHeight="360" minWidth="640" par="16:9" segmentAlignment="true" startWithSAP="1" ns2:qualityLevel="SD">
      <!-- Specifies which role is -->
      <Role schemeIdUri="urn:mpeg:dash:role:2011" value="main"/>
      <!-- Specifies how a segment is created -->
      <SegmentTemplate duration="6" initialization="$RepresentationID$/init.mp4" media="$RepresentationID$/$Number$.m4s" startNumber="0"/>
      <!-- Specifies -->
      <Representation bandwidth="300000" codecs="avc1.64001e" frameRate="60/2" height="360" id="V300" sar="1:1" width="640"/>
    </AdaptationSet>
  </Period>

  <!-- Mandatory for dynamic live streaming -->
  <UTCTiming schemeIdUri="urn:mpeg:dash:utc:http-head:2014" value="https://vm2.dashif.org/dash/time.txt"/>
</MPD>
