
import path from "path";
import fs from "fs";

import express from "express";
import { execute } from "./src/gstreamer";
import { getConfig } from "./src/config";
import { createOutputDirectory } from "./src/output";
import { createMPD } from "./src/mpd";
import { getVideoInfo } from "./src/mp4info";
import { start } from "./src/watcher";

const config = getConfig();

// Starts watcher
start(config);

// This will also remove and recreate the directory if config.output.clear is true.
createOutputDirectory(config);

// Executes gstreamer `gst-launch-1.0` to start the video streaming.
execute(config);

// We also need an HTTP server, so we create a Express application just
// to deliver video files and `index.html` file with the ShakaPlayer demo.
const app = express();

// We need to serve video static files.
app.use(express.static(config.output.path));
app.use("/vendor", express.static("./vendor"));

// 4000 by default but configurable in config.yaml or using env variable PORT.
app.set("port", config.server.port || process.env.PORT || 4000);

// Shaka Player html
app.get("/", (req, res) => {
	res.sendFile(path.resolve(__dirname, "index.html"));
});

// We need to generate the MPD manually.
app.get("/stream.mpd", (req, res) => {
  // When the config.videoInfo property is available (it is available
  // after retrieving info from `video_0.mp4` using `mp4info`) we can
  // generate a proper MPD for the stream.
	if (config.videoInfo) {
    const xml = createMPD(config);
		return res.set("Content-Type", "application/dash+xml")
							.send(xml);
	} else {
    // Because we don't have videoInfo, we send an empty MPD.
		return res.set("Content-Type", "application/dash+xml")
							.send(`<?xml version="1.0" ?><MPD></MPD>`);
	}
});

app.listen(app.get("port"), () => {
	console.log(`Listening on port \x1B[0;32m${app.get("port")}\x1B[0m`);
});
