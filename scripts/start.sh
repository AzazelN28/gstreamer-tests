#!/bin/sh

# Obtains the Operative System name.
OS=`uname`

# Fragment duration in Milliseconds
OPTIONS_FAST_START=1
OPTIONS_STREAMABLE=1
OPTIONS_USE_SPLITMUXSINK=1
OPTIONS_FRAGMENT_DURATION=60000
OPTIONS_FILENAME="video%04d.mp4"

# Source
SOURCE="videotestsrc is-live=true"

# Selects encoder depending on availability
if [[ $OS == "Darwin" ]]; then
  ENCODER="vtenc_h264"
else
  ENCODER="x264enc"
fi

MUXER="mp4mux faststart=${OPTIONS_FAST_START} streamable=${OPTIONS_STREAMABLE} fragment-duration=${OPTIONS_FRAGMENT_DURATION}"

if [[ $OPTIONS_USE_SPLITMUXSINK == 1 ]]; then
  DESTINATION="splitmuxsink location=${OPTIONS_FILENAME} muxer='${MUXER}'"
else
  DESTINATION="multifilesink location=${OPTIONS_FILENAME}"
fi

if [[ $OPTIONS_USE_SPLITMUXSINK == 1 ]]; then
  COMMAND="gst-launch-1.0 ${SOURCE} ! ${ENCODER} ! ${DESTINATION}"
else
  COMMAND="gst-launch-1.0 ${SOURCE} ! ${ENCODER} ! ${MUXER} ! ${DESTINATION}"
fi

echo "Executing \x1B[0;32m${COMMAND}\x1B[0m"

`${COMMAND}`
