import os from "os";
import cp from "child_process";
import printf from "printf";
import { seconds, minutes } from "./time";

/**
 * Returns which input source are we going to use in gstreamer.
 *
 * @param {Object} config
 * @return {String}
 */
function getInput(config) {
  if (config.input.type === "test") {
    return `videotestsrc is-live=true pattern=${config.input.pattern || "ball"}`;
  } else if (config.input.type === "file") {
    return `filesrc location='${config.input.path}' ! decodebin`;
  }
  throw new Error("Invalid input type");
}

/**
 * Creates the command string that we need to execute for a given config.
 *
 * @param {Object} config
 * @return {String}
 */
export function createCommandFromConfig(config) {
  const source = getInput(config);
	const pipeline = os.platform() === "darwin" ? "vtenc_h264" : "x264enc";
	const muxer = `mp4mux faststart=1 streamable=1 fragment-duration=${seconds(config.fragment.duration).asMilliseconds()}`;
	const destination = `splitmuxsink location=${config.output.path}/${config.output.pattern} muxer='${muxer}' max-size-time=${seconds(config.fragment.duration).asNanoseconds()}`;
	return `gst-launch-1.0 ${source} ! ${pipeline} ! ${destination}`;
}

/**
 * Spawns a child process executing GStreamer using a pipeline defined by
 * the given configuration.
 *
 * @param {Object} config
 * @return {ChildProcess} - Returns the spawned child process
 */
export function execute(config) {
  const command = createCommandFromConfig(config);
  console.log(`Executing command \x1B[0;32m${command}\x1B[0m`);
  return cp.exec(command, (err, stdout, stderr) => {
  	if (err) {
  		console.error(err);
  	} else {
  		console.log(stdout);
  	}
  });
}

/**
 * Returns the first chunk video filename.
 *
 * @param {Object} config
 * @return {String}
 */
export function getInitialFilename(config) {
	return `${config.output.path}/${printf(config.output.pattern, 0)}`;
}

export default {
  execute,
  createCommandFromConfig,
  getInitialFilename
}
