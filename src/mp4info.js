import cp from "child_process";

/**
 * Returns a promise that resolves into an Object with all the information
 * extracted from a video.
 *
 * @param {String} file - Path to video file
 * @return {Promise}
 */
export function getVideoInfo(file) {
  return new Promise((resolve, reject) => {
    cp.exec(`mp4info --format json ${file}`, (err, stdout, stderr) => {
      if (err) {
        console.error("Error", err);
      } else {
        const videoInfo = JSON.parse(stdout);
        return resolve(videoInfo);
      }
    });
  });
}

export default {
  getVideoInfo
}
