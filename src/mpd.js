import { minutes, seconds } from "./time";

export function createMPD(config) {
	const xml = `<?xml version="1.0"?>
<MPD
	xmlns="urn:mpeg:dash:schema:mpd:2011"
	type="dynamic"
	profiles="urn:mpeg:dash:profile:isoff-live:2011,urn:com:dashif:dash264"
	availabilityStartTime="${config.availabilityStartTime}"
	publishTime="${config.publishTime}"
	minBufferTime="PT5S"
	suggestedPresentationDelay="PT15S">
	<UTCTiming schemeIdUri="urn:mpeg:dash:utc:http-head:2014" value="https://vm2.dashif.org/dash/time.txt" />
	<Period start="PT00H00M0.00S">
		<AdaptationSet segmentAlignment="true">
			<SegmentTemplate timescale="1000" duration="${seconds(config.fragment.duration).asMilliseconds()}" />
			<Representation id="0" bandwidth="515348" width="${config.videoInfo.tracks[0].sample_descriptions[0].width}" height="${config.videoInfo.tracks[0].sample_descriptions[0].height}" mimeType="video/mp4" codecs="${config.videoInfo.tracks[0].sample_descriptions[0].codecs_string}">
				<SegmentTemplate media="video_$Number$.mp4" startNumber="0" />
			</Representation>
		</AdaptationSet>
	</Period>
</MPD>`;
  return xml;
}

export default {
  createMPD
}
