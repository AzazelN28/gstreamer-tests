import yaml from "js-yaml";
import path from "path";
import fs from "fs";
import assign from "deep-assign";

export function getConfig() {
	const defaultConfig = {
		debug: true,
    availabilityStartTime: (new Date()).toISOString(),
    publishTime: (new Date()).toISOString(),
    input: {
      type: "test",
      pattern: "smpte"
    },
		output: {
			path: "out",
			pattern: "video_%d.mp4",
			clear: true
		},
		server: {
			port: 4000
		},
		fragment: {
			duration: 60
		}
	};
	return assign(defaultConfig, yaml.safeLoad(fs.readFileSync("./config.yaml")));
}

export default {
  getConfig
}
