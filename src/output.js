import path from "path";
import fs from "fs";
import rimraf from "rimraf";

export function createOutputDirectory(config) {
	if (!fs.existsSync(config.output.path)) {
		return fs.mkdirSync(config.output.path);
	} else {
		if (config.output.clear) {
			rimraf.sync(path.join(config.output.path, "*.mp4"));
		}
	}
	return false;
}

export default {
  createOutputDirectory
}
