/**
 * Returns an object capable of transforming hours in other formats.
 *
 * @param {Number} value
 * @return {Object}
 */
export function hours(value) {
	return {
		asDays() { return value / 24; },
		asHours() { return value; },
		asMinutes() { return value * 60; },
		asSeconds() { return value * 3600; },
		asMilliseconds() { return value * 3600 * 1000; },
		asMicroseconds() { return value * 3600 * 1000000; },
		asNanoseconds()  { return value * 3600 * 1000000000; }
	};
}

/**
 * Returns an object capable of transforming minutes in other formats.
 *
 * @param {Number} value
 * @return {Object}
 */
export function minutes(value) {
	return {
		asDays() { return value / 1440; },
		asHours() { return value / 60; },
		asMinutes() { return value; },
		asSeconds() { return value * 60; },
		asMilliseconds() { return value * 60 * 1000; },
		asMicroseconds() { return value * 60 * 1000000; },
		asNanoseconds()  { return value * 60 * 1000000000; }
	};
}

/**
 * Returns an object capable of transforming seconds in other formats.
 *
 * @param {Number} value
 * @return {Object}
 */
export function seconds(value) {
	return {
		asDays() { return value / 86400; },
		asHours() { return value / 3600; },
		asMinutes() { return value / 60; },
		asSeconds() { return value; },
		asMilliseconds() { return value * 1000; },
		asMicroseconds() { return value * 1000000; },
		asNanoseconds()  { return value * 1000000000; }
	};
}

export default {
  seconds,
  minutes,
  hours
}
