import chokidar from "chokidar";
import { getInitialFilename } from "./gstreamer";
import { getVideoInfo } from "./mp4info";

/**
 * Starts a file watcher using the given config.
 *
 * @param {Object} config
 * @return {chokidar.Watcher}
 */
export function start(config) {

  function onAdd(file) {
  	console.log(`File \x1B[0;32m${file}\x1B[0m created`);
  }

  function onChange(file) {
  	console.log(`File \x1B[0;35m${file}\x1B[0m modified`);
  	// We need to obtain some video info so what we do is reading that data
  	// from the first video of the stream using `mp4info`.
  	if (file === getInitialFilename(config)) {
      getVideoInfo(file).then((videoInfo) => {
        // We save the returned info in our config object.
        config.videoInfo = videoInfo;
      });
  	}
  }

  // Starts a chokidar watcher to see when the files are being created and modified
  // to use `mp4info` to retrieve information about the streamed video.
  const watcher = chokidar.watch(config.output.path);
  watcher.on("add", onAdd);
  watcher.on("change", onChange);
  return watcher;
}

export default {
  start
}
